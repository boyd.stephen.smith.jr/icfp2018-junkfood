module Coordinate
	( Coordinate
	)
where

type Coordinate = (Int, Int, Int)
