{-# LANGUAGE Rank2Types #-}
module Decode
	( decode, hGetTrace
	)
where

-- base
import Control.Monad (forever)
import Data.Bits (Bits, unsafeShiftR, (.&.))
import Data.Word (Word8)
import System.IO (Handle)

-- bytestring
import qualified Data.ByteString as BS

-- pipes
import Pipes (lift, Producer', yield, Consumer', await, Pipe, (>->), each)

-- local
import Command
import Util

decodeND :: Word8 -> NearCoordinateDifference
decodeND nd = D (wordToInt x - 1) (wordToInt y - 1) (wordToInt z - 1)
 where
	(xy, z) = nd `divMod` 3
	(x, y) = xy `divMod` 3

sld :: Word8 {- Axis -} -> Word8 {- Length; "i" -} -> ShortLinearCoordinateDifference
sld 1 i = D (wordToInt i - 5) 0 0
sld 2 i = D 0 (wordToInt i - 5) 0
sld 3 i = D 0 0 (wordToInt i - 5)
sld _ _ = error "Invalid axis"

lld :: Word8 {- Axis -} -> Word8 {- Length; "i" -} -> LongLinearCoordinateDifference
lld 1 i = D (wordToInt i - 15) 0 0
lld 2 i = D 0 (wordToInt i - 15) 0
lld 3 i = D 0 0 (wordToInt i - 15)
lld _ _ = error "Invalid axis"

fd :: Word8 -> Word8 -> Word8 -> FarCoordinateDifference
fd dx dy dz = D (wordToInt dx - 30) (wordToInt dy - 30) (wordToInt dz - 30)

decode :: Monad m => Consumer' Word8 m Command
decode = do
	h <- await
	case h of
	 255 -> pure Halt
	 254 -> pure Wait
	 253 -> pure Flip
	 _ -> case h .&. 7 of
		 7 -> pure . FusionP . decodeND $ h .>>. 3
		 6 -> pure . FusionS . decodeND $ h .>>. 3
		 5 -> await >>= pure . Fission (decodeND $ h .>>. 3)
		 4 -> do
			t <- await
			case h .&. 8 of
			 8 -> pure $ LMove (sld (h .>>. 4 .&. 3) (t .&. 15)) (sld (h .>>. 6) (t .>>. 4 .&. 15))
			 0 -> pure . SMove $ lld (h .>>. 4 .&. 3) (t .&. 31)
			 _ -> error "Impossible; x .&. 8 `notElem` [8, 0]"
		 3 -> pure . Fill . decodeND $ h .>>. 3
		 2 -> pure . Void . decodeND $ h .>>. 3
		 1 -> GFill (decodeND $ h .>>. 3) <$> (fd <$> await <*> await <*> await)
		 0 -> GVoid (decodeND $ h .>>. 3) <$> (fd <$> await <*> await <*> await)
		 _ -> error "Impossible; x .&. 7 `notElem` [0 .. 7]"

streamDecode :: Monad m => Pipe Word8 Command m ()
streamDecode = forever $ decode >>= yield

fetchBytes :: Handle -> Int -> Producer' Word8 IO ()
fetchBytes h bsz = fetchBytes'
 where
	fetchBytes' = do
		dat <- lift $ BS.hGetSome h bsz
		if BS.null dat
		 then return ()
		 else each (BS.unpack dat) >> fetchBytes'

hGetTrace :: Handle -> Producer' Command IO ()
hGetTrace h = fetchBytes h 8000 >-> streamDecode
