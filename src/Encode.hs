{-# LANGUAGE RecordWildCards, Rank2Types #-}

module Encode
	( encode, writeTrace, streamWriteTrace
	)
where

-- base
import Control.Monad (forever)
import Data.Bits (Bits, unsafeShiftL, (.|.))
import Data.Word (Word8)

-- bytestring
import           Data.ByteString (ByteString)
import qualified Data.ByteString as BS

-- pipes
import Pipes (lift, runEffect, yield, Consumer', await, each, (>->))

-- local
import Command
import Util

(.<<.) :: Bits b => b -> Int -> b
(.<<.) = unsafeShiftL
infixl 8 .<<.

unsafeIntToWord8 :: Int -> Word8
unsafeIntToWord8 = fromInteger . toInteger

axis :: LinearCoordinateDifference -> Word8
axis (D _ 0 0) = 1
axis (D 0 _ 0) = 2
axis (D 0 0 _) = 3
axis _         = error "Not a Linear Coordinate Difference"

sldi :: ShortLinearCoordinateDifference -> Int
sldi (D dx 0 0) = dx + 5
sldi (D 0 dy 0) = dy + 5
sldi (D 0 0 dz) = dz + 5
sldi _          = error "Not a Linear Coordinate Difference"

lldi :: LongLinearCoordinateDifference -> Int
lldi (D dx 0 0) = dx + 15
lldi (D 0 dy 0) = dy + 15
lldi (D 0 0 dz) = dz + 15
lldi _          = error "Not a Linear Coordinate Difference"

encodeND :: NearCoordinateDifference -> Word8
encodeND D{..} = unsafeIntToWord8 $ (dx + 1) * 9 + (dy + 1) * 3 + (dz + 1)

encode :: Command -> ByteString
encode Halt = BS.singleton 255
encode Wait = BS.singleton 254
encode Flip = BS.singleton 253
encode (SMove lld) = BS.pack [axis lld .<<. 4 .|. 4, unsafeIntToWord8 $ lldi lld]
encode (LMove sld1 sld2) = BS.pack [axis sld2 .<<. 6 .|. axis sld1 .<<. 4 .|. 12, unsafeIntToWord8 $ sldi sld2 .<<. 4 .|. sldi sld1]
encode (FusionP nd) = BS.singleton $ encodeND nd .<<. 3 .|. 7
encode (FusionS nd) = BS.singleton $ encodeND nd .<<. 3 .|. 6
encode (Fission nd m) = BS.pack [encodeND nd .<<. 3 .|. 5, m]
encode (Fill nd) = BS.singleton $ encodeND nd .<<. 3 .|. 3
encode (Void nd) = BS.singleton $ encodeND nd .<<. 3 .|. 2
encode (GFill nd fd) = BS.pack [encodeND nd .<<. 3 .|. 1, unsafeIntToWord8 $ dx fd + 30, unsafeIntToWord8 $ dy fd + 30, unsafeIntToWord8 $ dz fd + 30]
encode (GVoid nd fd) = BS.pack [encodeND nd .<<. 3, unsafeIntToWord8 $ dx fd + 30, unsafeIntToWord8 $ dy fd + 30, unsafeIntToWord8 $ dz fd + 30]

writeTrace :: (Monad m, Foldable f) => (ByteString -> m ()) -> f Command -> m ()
writeTrace writeBytes f = runEffect $ each f >-> streamWriteTrace writeBytes

streamWriteTrace :: Monad m => (ByteString -> m ()) -> Consumer' Command m ()
streamWriteTrace writeBytes = forever $ await >>= lift . writeBytes . encode
