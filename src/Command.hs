module Command
	( Command(..)
	  -- Compatibility re-exports
	, CoordinateDifference(..)
	, LinearCoordinateDifference, Axis(..), ld
	, ShortLinearCoordinateDifference, SLD
	, LongLinearCoordinateDifference, LLD
	, NearCoordinateDifference, ND
	, FarCoordinateDifference, FD
	)
where

-- base
import Data.Word (Word8)

-- (this package)
import CoordinateDifference

data Command = Halt
             | Wait
             | Flip
             | SMove {-# UNPACK #-} !LLD
             | LMove {-# UNPACK #-} !SLD {-# UNPACK #-} !SLD
             | FusionP{-rimary-} {-# UNPACK #-} !ND
             | FusionS{-econdary-} {-# UNPACK #-} !ND
             | Fission {-# UNPACK #-} !ND !Word8
             | Fill {-# UNPACK #-} !ND
             | Void {-# UNPACK #-} !ND
             | GFill {-# UNPACK #-} !ND {-# UNPACK #-} !FD
             | GVoid {-# UNPACK #-} !ND {-# UNPACK #-} !FD
             deriving (Eq, Show, Read)
