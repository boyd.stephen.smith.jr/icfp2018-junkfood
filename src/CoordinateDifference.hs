module CoordinateDifference
	( CoordinateDifference(..)
	, LinearCoordinateDifference, Axis(..)
	, ShortLinearCoordinateDifference, SLD
	, LongLinearCoordinateDifference, LLD
	, NearCoordinateDifference, ND
	, FarCoordinateDifference, FD
	, ld
	, (.+), addCoordinateDifference
	, (.-), subtractCoordinates
	, mlen, clen
	)
where

-- base
import Data.Word (Word8)

-- (this package)
import Coordinate
import Util

data CoordinateDifference = D
	{ dx :: Int
	, dy :: Int
	, dz :: Int
	} deriving (Eq, Show, Read)

type LinearCoordinateDifference = CoordinateDifference

data Axis = X | Y | Z

ld :: Axis -> Int -> LinearCoordinateDifference
ld X i = D i 0 0
ld Y i = D 0 i 0
ld Z i = D 0 0 i

type ShortLinearCoordinateDifference = LinearCoordinateDifference
type SLD = ShortLinearCoordinateDifference

type LongLinearCoordinateDifference = LinearCoordinateDifference
type LLD = LongLinearCoordinateDifference

type NearCoordinateDifference = CoordinateDifference
type ND = NearCoordinateDifference

type FarCoordinateDifference = CoordinateDifference
type FD = FarCoordinateDifference

(.+), addCoordinateDifference :: Coordinate -> CoordinateDifference -> Coordinate
(x, y, z) .+ (D dx dy dz) = (x + dx, y + dy, z + dz)
addCoordinateDifference = (.+)
infixl 6 .+

(.-), subtractCoordinates :: Coordinate -> Coordinate -> CoordinateDifference
(x1, y1, z1) .- (x2, y2, z2) = D (x2 - x1) (y2 - y1) (z2 - z1)
subtractCoordinates = (.-)
infixl 6 .-

mlen :: CoordinateDifference -> Int
mlen (D dx dy dz) = abs dx + abs dy + abs dz

clen :: CoordinateDifference -> Int
clen (D dx dy dz) = max (abs dx) . max (abs dy) $ abs dz
