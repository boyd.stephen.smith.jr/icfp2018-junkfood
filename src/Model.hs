{-# LANGUAGE RecordWildCards #-}
module Model
	( Model, resolution, matrix
	, Coordinate -- re-export
	, at
	, hGetModel
	)
where

-- base
import Data.Bits (testBit, unsafeShiftR, (.&.))
import Data.Word (Word8)
import System.IO (Handle)

-- bytestring
import           Data.ByteString (ByteString, index, hGet)
import qualified Data.ByteString as BS

-- local
import Coordinate
import Util

data Model = Model
	{ res :: Int
	, matrix :: ByteString
	}

resolution :: Model -> Int
resolution Model{..} = res

at :: Coordinate -> Model -> Bool
at (x, y, z) Model{..} = testBit (index matrix byte) bit
 where
	ix = x * res * res + y * res + z
	byte = ix .>>. 3
	bit = ix .&. 7

hGetModel :: Handle -> IO Model
hGetModel h = do
  res <- wordToInt . BS.head <$> hGet h 1
  let sz = (res * res * res + 7) .>>. 3
  Model res <$> hGet h sz
